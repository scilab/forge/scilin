Scilab - NII Project 

March the 9th 2011

Michaël Baudin

Abstract
--------

In this document, we present a list of tasks to perform in 
order to integrate the NII solvers into the Scilin project. 
The goal of this project is to provide a set of preconditionners and 
algorithms for the resolution of sparse linear systems of equations 
by iterative methods. 
Indeed, the NII (National Institute of Informatics) is a leading 
research institute on sparse iterative methods for linear least 
squares. 
The main steps of this projects involve a bibliography of the 
algorithms and tools, an update of the Scilin module and the 
integration of the NII preconditionners and algorithms. 
We provide a brief roadmap for these tasks and present 
the expected results and the time required to perform them.

Introduction
------------

Scilab provides several features to manage sparse matrices and perform
usual linear algebra operations on them. 
These operations includes all the
basic linear algebra including addition, dot product, transpose and the matrix
vector product. 
For these basic operations, we do not make any dierence in
a script managing a dense matrix and a script managing a sparse matrix.
Higher level operations include solving linear systems of equations, com-
puting some of their eigenvalues or computing some decompositions (e.g.
Cholesky). 
The usual functions, such as the lu or chol functions, do not
work with sparse matrices and special sparse functions must be used.

The current set of tools available in Scilab for sparse matrices are the 
following:
 * management of sparse matrices of arbitrary size,
 * basic algebra on sparse matrices, including, sum, dot product, 
   transpose, matrix-matrix product,
 * sparse LU decomposition and resolution of linear equations from this 
   decomposition based on the Sparse package written by Kenneth S. Kundert 
   and Alberto Sangiovanni-Vincentelli,
 * sparse Cholesky decomposition and resolution of linear equations from this 
   decomposition, based on a sparse Cholesky factorization package developed 
   by Esmond Ng and Barry Peyton at ORNL and a multiple minimun-degree 
   ordering package by Joseph Liu at University of Waterloo,
 * iterative methods of linear systems of equations, including Pre-Conditionned 
   Conjugate Gradient (pcg), Generalized Minimum Residual method (gmres), 
   Quasi Minimal Residual method with preconditionning (qmr),
 * sparse LU decomposition and resolution of linear equations from this 
   decomposition based on the UMFPACK library,
 * sparse Cholesky decomposition and resolution of linear equations from this 
   decomposition based on the TAUCS library,
 * sparse eigenvalue computations, based on the Arpack library, 
   using the Implicitly Restarted Arnoldi Method.

Several file formats allows to manage sparse matrices. 
Scilab is able to read matrices in the Harwell-Boeing format, thanks to the 
ReadHBSparse function of the Umfpack module.
The Matrix Market external module (available in ATOMS) provides functions to read 
and write matrices in the Matrix Market format. 

The main challenges of Scilab in the field of sparse matrices are the 
following. 
The first challenge is to provide preconditionners for iterative methods. 
Indeed, Scilab provides iterative methods, but does not provide 
preconditionners for these methods (pcg, gmres, qmr). 
Since these algorithms perform much better when preconditionned, 
this is a primary goal. 
The second challenge is to provide algorithms for linear least squares 
problems. 

Ken Hayami is a Professor at the National Institute of Informatics (NII), 
within the Principles of Informatics Research Division.
He leads a research project on sparse iterative methods for linear least 
squares problems. 
With Xiaoke Cui, Jun-Feng Yin and Keiichi Morikuni, he developped 
preconditionners and iterative algorithms for the resolution 
of sparse linear least squares problems. 
These algorithms are based on 3 solvers. 
The methods are based on Robust Incomplete Factorization (RIF) with 
GMRES algorithm, Greville type preconditioner with GMRES or 
inner iteration preconditioners (Jacobi and SOR type). 
These algorithms are available either in C/C++ or in Fortran. 
They are well suited to rank deficient or full rank problems, 
ill-conditioned, over or underdetermined, large sparse problems, 
have various levels of memory requirements and various performances.

Scilin is a Scilab toolbox for sparse linear systems. 
It provides preconditionners and iterative methods for the sparse 
linear equations.
Scilin provides :
 * Incomplete LU factorisations,
 * Iterative solvers.
The Scilin project provides 9 iterative solvers (bicg, bicgstab, etc...), 
7 preconditionners (ilut, ilutp, etc...), 
and 2 extra functions (det, nnz).
This module has been developed by Aladin Group (IRISA-INRIA) in 2000-2001, 
from a basic toolkit for sparse matrix computations, SPARSKIT (developed by Y.Saad, University of Minnesota) 
and from the Mltemplates library. 
The Scilin module was developped for Scilab 4 and does not work in Scilab 5. 

The main goal of this project is to merge all these tools for sparse matrices 
into a module which would be available in Scilab 5. 
We plan to update the Scilin module so that it can be available in Scilab 5. 
This corresponds to 14 functions to be updated. 
We plan to release the source codes from the NII under the CeCILL license and 
to connect them into Scilab. 
This corresponds to 3 functions to be created. 

Schedule
--------

In this section, we analyze the tasks which needs to be done and the associated 
schedule. 
We present an estimate of the time required for each step. 

The four main tasks are:
 * Bibliography: 
   establish the current state of the tools, 
   analyze the various source codes and algorithms, and their compatibility.
 * Update Scilin:
   update the file formats, so that the Scilin module is 
   available in Scilab 5.
 * Connect the Least Squares solvers from NII:
   create the gateways, create the help pages, the tests.
 * Integration:
   validate the tools, create the ATOMS module, 
   apply on specific test cases, documentation.

We now give a detailed review of the steps required in this 
project and the questions which need answers. 

 * Bibliography
   * Analyze the sparse matrix format in Scilab. 
     What is the library used in Scilab for the storage of sparse matrices ?
     What is the sparse matrix format used in Scilab ?
   * Analyze the sparse matrix format in the Least Squares (L.S.) solvers ?
     What is the library used in the L.S. solvers for the storage of sparse matrices ?
     What is the sparse matrix format used in L.S. solvers ?
   * Analyze the compatibility of the sparse matrix formats.
     Are the matrix formats used in Scilab and Scilin compatibles ?
     Are the matrix formats used in the L.S. solvers and Scilin compatibles ?
     Are the matrix formats used in the L.S. solvers and Scilab compatibles ?
     Are there changes to be done in the sparse matrix format of Scilab, Scilin or 
     the L.S. solvers ?
     Are there sparse matrix format conversion functions to be created ?
   * What are the features provided by the L.S. solvers ?
     Write the header of the functions to be provided in Scilab. 
     Write the type, size and content of the input and output arguments of the 
     functions to be provided.
   * Can the algorithms be used with other preconditionners ?
   * Can the L.S. preconditionners be separated from the L.S. algorithms ?
   * How will the new L.S. solvers will integrate with the existing functions ?
     How will the new L.S. functions will be used in combination with Scilab functions ?
     How will the new L.S. functions will be used in combination with Scilin functions ?
   * Can the L.S. functions be integrated ?
     Must the source code be rewritten from Fortran to C ?
     Must the source code be rewritten from C++ to C ?
     Must the source code be rewritten from Matlab to C ?
     Can the pre-requirements Sparse Lib ++ and MC++ be replaced by libraries from Scilab ?
     Can the pre-requirements Sparse Lib ++ and MC++ be compiled on Linux, on Windows ?
   * Update this schedule.
     Must the planning be updated ?
     Are there new tasks to be added ?
     Is the project feasible ?
   * Improve the technical report "Introduction to sparse matrices in Scilab"
     Create a section on the internal sparse matrix format.
     Create a section on the sparse matrix operations available in Scilab.
     Create a section on the Scilin module.
     Create a section on the L.S. solvers.

 * Update Scilin
   * Compile the library.
     Create the builders to compile the sources of the library.
     Compile on Windows 32 bits, Linux 32 bits with the builders.
     Load the library in Scilab on Windows, on Linux.
     Are the libraries cross-platform, i.e. do they compile on Windows, on Linux ?     
   * Compile the gateways.
     Create the builders to compile the gateways of the library.
     Compile on Windows 32 bits, Linux 32 bits with the builders.
     Load the library in Scilab on Windows, on Linux.
     Are the gateways cross-platform, i.e. do they compile on Windows, on Linux ?
     Are there language features which are not portable (e.g. C99) ?
     Update the gateways to use the Scilab 5 API. 
     Take into account for localization constraints.
     Are the gateways to be massively updated ?
     Are there gateways which are provided but are not designed to be 
     public (private functions) ?
   * Compile the macros.
     Are the macros compatible with Scilab 5 ?
     Are there missing functions in the macros ?
     Take into account for localization constraints, using standard messages.
     Update the macros for input argument checking, using the apifun module.
     Are there macros which are provided but are not designed to be 
     public (private functions) ?
     Remove the functions which have been integrated into Scilab 5:
     the Matrix-Market functions (mminfo, mmread and mmwrite), 
     the iterative solvers (pcg, gmres and qmr).
   * Update or create the help pages.
     Convert the man pages into .xml.
     Check that the headers are well described.
     Improve the description of input and output arguments, following the 
     Scilab standards.
     Improve or create the examples.
     Create help pages for undocumented functions.
   * Update or create the demos.
     Create the demo gateway.
     Create or update the existing demos and connect them to the demos.
   * Create or update the unit tests.
     Does the function perform their work ?
     Exercize all the features in the unit tests.
   * Release an alpha version.
     Release intermediate zip containing the module (typically each week).
     Review the compilation, gateways, macros, help pages, tests and demos.
     Create the ATOMS module.
     Review the packaging process on Windows 64 bits, Linux 64 bits, Mac.
     Check the installation and unit tests on several plaforms.
     Review the help pages, tests and demos.
   * Improve the technical report "Introduction to sparse matrices in Scilab"
     Update the section on the Scilin module.

 * Connect the Least Squares solvers from NII into Scilin.
   * Create the 3 gateways.
   * Create the 3 help pages.
   * Create the 3 unit tests.
   * Create the demos.
     Reproduce the tests from the papers and technical reports. 
   * Release a beta version.
   * Improve the technical report "Introduction to sparse matrices in Scilab"
     Update a section on the Least Squares solvers from Scilin.

 * Integration:
   * release a v1.0 version.
   * create the ATOMS module
   * validate the tools
   * apply on specific test cases
   * Improve the technical report "Introduction to sparse matrices in Scilab"
     Add a demo of the new tools.
   * release a v1.1 version.

We now present the outputs of the four tasks and the time 
required to perform them (minimum and maximum bounds).

 * Bibliography
   Outputs : updated technical report.
   Time: [1 weeks , 4 weeks]
 * Update Scilin
   Outputs: updated Scilin ATOMS module for Scilab 5, 
            updated technical report.
   Time: [3 weeks, 8 weeks]
 * Connect the Least Squares solvers from NII into Scilin.
   Outputs: updated Scilin ATOMS module for Scilab 5 with L.S. features, 
            updated technical report.
   Time: [2 weeks, 4 weeks]
 * Integration:
   Outputs: updated Scilin ATOMS module for Scilab 5 with L.S. features, 
            updated technical report.
   Time: [2 weeks, 4 weeks]
 * Total : [8 weeks , 20 weeks]

We now analyze the risks in this project. 
 * After the Bibliography step, this schedule should be updated.
 * Updating the Scilin module might prove to be unfeasible.
 * The student might have problems to master the structure of a Scilab module.
 * Compiling the Scilin or the L.S. solvers on Windows may be difficult.
 * There might be problems with the dependencies of the L.S. solvers, 
   especially with Sparse Lib ++ or MV ++.
 * It might be necessary to convert the Fortran or Matlab sources 
   into the C language.

Bibliography
------------

"Introduction to sparse matrices in Scilab", 2010, Michael Baudin, DIGITEO, http://forge.scilab.org/upload/docscisparse/



