function [ys] = standard(x,y,n,inc)
//
//  Standardisation de fichiers de points :
//  interpolation lineaire entre les points
//  Par hypothese : les composantes de x sont entieres
//  comprises entre 1 et n et croissantes ; 
//  y est de meme longueur que x
//  En sortie les deux tableaux sont etendus a [1,n]

    m= max(size(x)) ;
    ys=zeros(1,n) ;
    ys(1:x(1)) = y(1) ; // premiere valeur etendue a gauche
    ys(x(m)+1:n) = y(m) ; // derniere valeur etendue a droite
    for k=1:m-1,
	lk=x(k+1)-x(k) ;
	if (lk > 1),
	   pente=(y(k+1)-y(k))/lk ;
	   ys(x(k)+1:x(k+1)-1)=y(k)+pente*[1:lk-1] ;
	end ;
	ys(x(k+1)) = y(k+1) ;
    end ;
endfunction
