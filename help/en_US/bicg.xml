<?xml version="1.0" encoding="ISO-8859-1"?><refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="bicg"><info><pubdate>March 2000  </pubdate></info><refnamediv><refname>bicg</refname><refpurpose> biconjugate gradient method with preconditioning.  </refpurpose></refnamediv>
  
   
  
   
  
   
  
   
  
   
  
   <refsynopsisdiv><title>Calling Sequence</title><synopsis>[x,err,iter,flag,res] = bicg(A,Ap,b,x0,M,Mp,maxi,tol)</synopsis></refsynopsisdiv>
  
   <refsection><title>Parameters</title>
 
      <variablelist>
  
         <varlistentry>
  
            <term>A  </term>
  
            <listitem>
    : matrix of size n-by-n or function returning <literal>A*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>Ap  </term>
  
            <listitem>
    : exists if <literal>A</literal> is a function. In this case <literal>Ap</literal> is the transposed function of <literal>A</literal>, i.e returns <literal>A'*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>b  </term>
  
            <listitem>
    : right hand side vector
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>x0  </term>
  
            <listitem>
    : initial guess vector (default: zeros(n,1))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>M  </term>
  
            <listitem>
    : preconditioner: matrix or function returning <literal>M*x</literal> (In the first case, default: eye(n,n))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>Mp  </term>
  
            <listitem>
    : must only be provided when <literal>M</literal> is a function. In this case <literal>Mp</literal> is the function which returns <literal>M'*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>maxi  </term>
  
            <listitem>
    : maximum number of iterations (default: n)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>tol  </term>
  
            <listitem>
    : error tolerance (default: 1000*%eps)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>x  </term>
  
            <listitem>
    : solution vector
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>err  </term>
  
            <listitem>
    : final residual norm
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>iter  </term>
  
            <listitem>
    : number of iterations performed
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>flag  </term>
  
            <listitem>
    : 0 = <literal>bicg</literal> converged to the desired tolerance within <literal>maxi</literal> iterations
  </listitem> 
  
         </varlistentry>
  
         <para>
       1 = no convergence given <literal>maxi</literal>
  
         </para>
  
         <para>
      -1 = breakdown
  </para>
  
         <varlistentry>
  
            <term>res  </term>
  
            <listitem>
    : residual vector
  </listitem> 
  
         </varlistentry>
 
      </variablelist>
  
   </refsection>
  
   <refsection><title>Description</title>
  
      <para>
    Solves the linear system <literal>Ax=b</literal> using the BiConjugate Gradient Method with preconditioning.
  </para>
  
   </refsection>
  
   <refsection><title>Examples</title><programlisting role="example"><![CDATA[
A=makefish(4); b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = bicg(A,b,x0)
M=eye(16,16); max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = bicg(A,b,x0,M,max_it,tol)

deff("y=precond(x)","y=(M+eye(size(M,1),size(M,2)))*x");
deff("y=precondp(x)","y=(M+eye(size(M,1),size(M,2)))''*x");

deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");
deff("y=matvecp(x)","y=(A+eye(size(A,1),size(A,1)))''*x");

[x,err,iter,flag,res] = bicg(matvec,matvecp,b,x0,precond,precondp,max_it,tol)

[x,err,iter,flag,res] = bicg(matvec,matvecp,b,x0,M,max_it,tol)
[x,err,iter,flag,res] = bicg(A,b,x0,precond,precondp,max_it,tol)

 ]]></programlisting></refsection>
  
   <refsection><title>Authors</title><para>Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.  </para></refsection>
  
   <refsection><title>See Also</title><simplelist type="inline">
    
      <member> 
         <link linkend="bicgstab">bicgstab</link> 
      </member>     
      <member> 
         <link linkend="cgs">cgs</link> 
      </member>     
      <member> 
         <link linkend="gmres">gmres</link> 
      </member>     
      <member> 
         <link linkend="qmr">qmr</link> 
      </member>
  
   </simplelist></refsection>

</refentry>