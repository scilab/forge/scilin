.TH ilu0 G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
ilu0 - simple ILU(0) preconditioning
.SH CALLING SEQUENCE
.nf
[L,U,ierr]=ilu0(A)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be square)
.TP
L
: lower triangular matrix
.TP
U
: upper triangular matrix
.TP
ierr
: error flag
.SH DESCRIPTION
Builds an incomplete LU factorization of the sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored in CSR format. The CSR format is the Compressed Sparse Row format used by Scilab. ILU0 is not recommended for realistic problems. It is only provided for comparison purposes.
.TP 
ierr
: The error flag on return.
.LP
   0 --> normal return
.LP
   k --> code stoped on a zero pivot at step k.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/nos1.mtx')
n=size(A,1); b=ones(n,1);
[L,U,ierr]=ilu0(A)
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
iluk, ilut, ilud, ilutp, iludp, milu0

