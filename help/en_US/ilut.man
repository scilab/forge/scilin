.TH ilut G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
ilut - sparse incomplete LU factorization with dual truncation
.SH CALLING SEQUENCE
.nf
[L,U,ierr]=ilut(A,lfil,drop)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be square)
.TP
lfil
: the fill-in parameter(default: average number of nonzero entries per rows)
.TP
drop
: the threshold for dropping small terms in the factorization; (default: 0.001*max(Aij))
.TP
L
: lower triangular sparse matrix
.TP
U
: upper triangular sparse matrix
.TP
ierr
: error flag
.SH DESCRIPTION
Builds an incomplete LU factorization of the sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored in CSR format. The CSR format is the Compressed Sparse Row format used by Scilab.
.LP
The diagonal elements of the input matrix must be nonzero (at least 'structurally')
.TP 
lfil
: each row of \fBL\fP and each row of \fBU\fP will have a maximum of \fBlfil\fP elements (excluding the diagonal element). \fBlfil\fP must be .ge. 0.
.TP
drop
: sets the threshold for dropping small terms in the factorization. The dual dropping strategy works as follow:
.LP
1) Thresholding in \fBL\fP and \fBU\fP as set by \fBdrop\fP. Any element whose magnitude is less than some tolerance (relative to the abs value of diagonal element in \fBU\fP) is dropped.
.LP
2) Keeping only the largest \fBlfil\fP elements in the i-th row of \fBL\fP and the largest \fBlfil\fP elements in the i-th row of \fBU\fP (excluding diagonal elements).
.LP
Flexibility: one can use \fBdrop=0\fP to get a strategy based on keeping the largest elements in each row of \fBL\fP and \fBU\fP. Taking \fBdrop\fP .ne. 0 but \fBlfil\fP=n, will give threshold strategy (however, fill-in is then mpredictible).
.TP
ierr
: error flag.
.LP
   0    --> successful return.
.LP
  >0  --> zero pivot encountered at step number ierr.
.LP
  -1   --> error. input matrix may be wrong. (The elimination process has generated a row in \fBL\fP or \fBU\fP whose length is .gt.  n.)
.LP
  -2   --> storage of matrix \fBL\fP caused an overflow in array al.
.LP
  -3   --> storage of matrix \fBU\fP caused an overflow in array alu.
.LP
  -4   --> illegal value for \fBlfil\fP.
.LP
  -5   --> zero row encountered.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/nos1.mtx')
n=size(A,1); b=ones(n,1);
lfil=10;drop=0;
[L,U,ierr]=ilut(A,lfil,drop);
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
ilu0, milu0, iluk, ilud, ilutp, iludp
