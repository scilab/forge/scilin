.TH lehmer G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
lehmer - a symmetric positive definite N-by-N matrix
.SH CALLING SEQUENCE
.nf
[A] = lehmer(n)
.fi
.SH PARAMETERS
.TP
n
: size of the matrix \fBA\fP
.TP
A
: matrix
.SH DESCRIPTION
Makes a symmetric positive definite N-by-N matrix with A(i,j) = i/j for j>=i. \fBA\fP is totally nonnegative.  INV(A) is tridiagonal, and explicit formulas are known for its entries. N <= COND(A) <= 4*N*N. 
.SH EXAMPLE
n=10;
[A] = lehmer(n)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
.SH SEE ALSO
makefish, matgen , wathen 