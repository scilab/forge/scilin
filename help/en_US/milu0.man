.TH milu0 G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
milu0 - MILU(0) preconditioning
.SH CALLING SEQUENCE
.nf
[L,U,ierr]=milu0(A)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be square)
.TP
L
: lower triangular sparse matrix
.TP
U
: upper triangular sparse matrix
.TP
ierr
: error flag
.SH DESCRIPTION
Builds an incomplete LU factorization of the sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored in the internal CSR format. The CSR format is the Compressed Sparse Row format used by Scilab. 
.TP 
ierr
: error flag
.LP
   0 --> normal return
.LP
   k --> code stoped on a zero pivot at step k.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/nos3.mtx')
n=size(A,1); b=ones(n,1);
[L,U,ierr]=milu0(spa)
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
ilu0, iluk, ilut, ilud, ilutp, iludp