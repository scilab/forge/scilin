.TH mmwrite G "May 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
mmwrite - writes the sparse or dense matrix A to a Matrix Market (MM) formatted file.
.SH CALLING SEQUENCE
.nf
[err] = mmwrite(file,A,comm,typ,prec)
.fi
.SH PARAMETERS
.TP
file
: destination file
.TP
A
: sparse or full matrix
.TP
comm
: matrix containing comments by rows (default: ''). For example:
.LP
  comment =['Comment 1';'Comment 2';'and so on.']
.TP
typ
: type of the data: 'real', 'complex', 'integer', 'pattern'. If ommitted, the type 'real' is used.
.TP
prec
: number of digits to display for real or complex values. If ommitted, full working precision is used.
.TP
err
: error flag. If all operations are correctly passed, \fBerr\fP = 0 

.SH DESCRIPTION
Writes the sparse or dense matrix A to a Matrix Market (MM) formated file. The arguments: \fBcomm\fP, \fBtyp\fP, and \fBprec\fP, are optional. The \fBcomm\fP argument is used in order to include comments in the destination file, but these comments are not taken into account by the routine 'mmread'.
.LP
This function only writes in the general format. No symmetries are used to reduce the matrix storage.
.LP
.SH EXAMPLE
.nf
A=makefish(4);
comm = str2mat(' This', ' is a', ' test matrix', '14/05/01');
[err] = mmwrite(file,A,comm)

.fi
.SH AUTHOR
Adaptation by Aladin of a source obtained from the Matrix Market web site - 14 May 2001.
.SH SEE ALSO
mmread, mminfo