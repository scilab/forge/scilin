.TH pcg G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
pcg - conjugate gradient method with preconditioning
.SH CALLING SEQUENCE
.nf
[x,err,iter,flag,res] = pcg(A,b,x0,M,maxi,tol)
.fi
.SH PARAMETERS
.TP
A
: symmetric positive definite matrix or function returning \fBA*x\fP
.TP
b
: right hand side vector
.TP
x0
: initial guess vector (default: zeros(n,1))
.TP
M
: preconditioner: matrix or function returning \fBM*x\fP (In the first case, default: eye(n,n))
.TP
maxi
: maximum number of iterations (default: n)
.TP
tol
: error tolerance (default: 1000*%eps)
.TP
x
: solution vector
.TP
err
: final residual norm
.TP
iter
: number of iterations performed
.TP
flag
: 0 = \fBpcg\fP converged to the desired tolerance within \fBmaxi\fP iterations
.LP
   1 = no convergence given \fBmaxi\fP
.TP
res
: residual vector
.SH DESCRIPTION
Solves the linear system \fBAx=b\fP using the Conjugate Gradient method with preconditioning.
.LP
The \fBA\fP matrix must be a symmetric positive definite matrix.
.SH EXAMPLE
.nf
A=lehmer(16);
b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = pcg(A,b,x0)
M=eye(16,16); max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = pcg(A,b,x0,M,max_it,tol)

deff("y=precond(x)","y=(M+eye(size(M,1),size(M,2)))*x");
deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = pcg(matvec,b,x0,precond,max_it,tol)

[x,err,iter,flag,res] = pcg(A,b,x0,precond)
[x,err,iter,flag,res] = pcg(matvec,b,x0,M)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
cheby
