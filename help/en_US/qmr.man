.TH qmr G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
qmr - quasi minimal resiqual method with preconditioning
.SH CALLING SEQUENCE
.nf
[x,err,iter,flag,res] = qmr(A,b,x0,M1,M1p,M2,M2p,maxi,tol)
.fi
.SH PARAMETERS
.TP
A
: matrix of size n-by-n or function returning \fBA*x\fP
.TP
b
: right hand side vector
.TP
x0
: initial guess vector (default: zeros(n,1))
.TP
M1
: left preconditioner: matrix or function returning \fBM1*x\fP (In the first case, default: eye(n,n))
.TP
M1p
: must only be provided when \fBM1\fP is a function. In this case \fBM1p\fP is the function which returns \fBM1'*x\fP
.TP
M2
: right preconditioner: matrix or function returning \fBM2*x\fP (In the first case, default: eye(n,n))
.TP
M2p
:  must only be provided when \fBM2\fP is a function. In this case \fBM2p\fP is the function which returns \fBM2'*x\fP
.TP
maxi
: maximum number of iterations (default: n)
.TP
tol
: error tolerance (default: 1000*%eps)
.TP
x
: solution vector
.TP
err
: final residual norm
.TP
iter
: number of iterations performed
.TP
flag
: 0 = \fBqmr\fP converged to the desired tolerance within \fBmaxi\fP iterations
.LP
   1 = no convergence given \fBmaxi\fP
.TP
res
: residual vector
.SH DESCRIPTION
Solves the linear system \fBAx=b\fP using the Quasi Minimal Residual Method with preconditioning.
.SH EXAMPLE
.nf
A=makefish(4); b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = qmr(A,b,x0)
M1=eye(16,16); M2=M1; max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = qmr(A,b,x0,M1,M2,max_it,tol)

deff("y=precond_g(x)","y=(M1+eye(size(M1,1),size(M1,2)))*x");
deff("y=precondp_g(x)","y=(M1+eye(size(M1,1),size(M1,2)))''*x");

deff("y=precond_d(x)","y=(M2+6*eye(size(M2,1),size(M2,2)))*x");
deff("y=precondp_d(x)","y=(M2+eye(size(M2,1),size(M2,2)))''*x");

deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = qmr(A,b,x0,precond_g,precondp_g,M2,max_it,tol)
[x,err,iter,flag,res] = qmr(matvec,b,x0,precond_g,precondp_g,precond_d,precondp_d)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
bicg, bicgstab, cgs, gmres