<?xml version="1.0" encoding="ISO-8859-1"?><refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="qmr"><info><pubdate>March 2000  </pubdate></info><refnamediv><refname>qmr</refname><refpurpose> quasi minimal resiqual method with preconditioning  </refpurpose></refnamediv>
  
   
  
   
  
   
  
   
  
   
  
   <refsynopsisdiv><title>Calling Sequence</title><synopsis>[x,err,iter,flag,res] = qmr(A,b,x0,M1,M1p,M2,M2p,maxi,tol)</synopsis></refsynopsisdiv>
  
   <refsection><title>Parameters</title>
 
      <variablelist>
  
         <varlistentry>
  
            <term>A  </term>
  
            <listitem>
    : matrix of size n-by-n or function returning <literal>A*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>b  </term>
  
            <listitem>
    : right hand side vector
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>x0  </term>
  
            <listitem>
    : initial guess vector (default: zeros(n,1))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>M1  </term>
  
            <listitem>
    : left preconditioner: matrix or function returning <literal>M1*x</literal> (In the first case, default: eye(n,n))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>M1p  </term>
  
            <listitem>
    : must only be provided when <literal>M1</literal> is a function. In this case <literal>M1p</literal> is the function which returns <literal>M1'*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>M2  </term>
  
            <listitem>
    : right preconditioner: matrix or function returning <literal>M2*x</literal> (In the first case, default: eye(n,n))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>M2p  </term>
  
            <listitem>
    :  must only be provided when <literal>M2</literal> is a function. In this case <literal>M2p</literal> is the function which returns <literal>M2'*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>maxi  </term>
  
            <listitem>
    : maximum number of iterations (default: n)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>tol  </term>
  
            <listitem>
    : error tolerance (default: 1000*%eps)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>x  </term>
  
            <listitem>
    : solution vector
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>err  </term>
  
            <listitem>
    : final residual norm
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>iter  </term>
  
            <listitem>
    : number of iterations performed
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>flag  </term>
  
            <listitem>
    : 0 = <literal>qmr</literal> converged to the desired tolerance within <literal>maxi</literal> iterations
  </listitem> 
  
         </varlistentry>
  
         <para>
       1 = no convergence given <literal>maxi</literal>
  
         </para>
  
         <varlistentry>
  
            <term>res  </term>
  
            <listitem>
    : residual vector
  </listitem> 
  
         </varlistentry>
 
      </variablelist>
  
   </refsection>
  
   <refsection><title>Description</title>
  
      <para>
    Solves the linear system <literal>Ax=b</literal> using the Quasi Minimal Residual Method with preconditioning.
  </para>
  
   </refsection>
  
   <refsection><title>Examples</title><programlisting role="example"><![CDATA[
A=makefish(4); b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = qmr(A,b,x0)
M1=eye(16,16); M2=M1; max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = qmr(A,b,x0,M1,M2,max_it,tol)

deff("y=precond_g(x)","y=(M1+eye(size(M1,1),size(M1,2)))*x");
deff("y=precondp_g(x)","y=(M1+eye(size(M1,1),size(M1,2)))''*x");

deff("y=precond_d(x)","y=(M2+6*eye(size(M2,1),size(M2,2)))*x");
deff("y=precondp_d(x)","y=(M2+eye(size(M2,1),size(M2,2)))''*x");

deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = qmr(A,b,x0,precond_g,precondp_g,M2,max_it,tol)
[x,err,iter,flag,res] = qmr(matvec,b,x0,precond_g,precondp_g,precond_d,precondp_d)
 ]]></programlisting></refsection>
  
   <refsection><title>Authors</title><para>Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.  </para></refsection>
  
   <refsection><title>See Also</title><simplelist type="inline">
    
      <member> 
         <link linkend="bicg">bicg</link> 
      </member>     
      <member> 
         <link linkend="bicgstab">bicgstab</link> 
      </member>     
      <member> 
         <link linkend="cgs">cgs</link> 
      </member>     
      <member> 
         <link linkend="gmres">gmres</link> 
      </member>
  
   </simplelist></refsection>

</refentry>