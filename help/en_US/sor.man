.TH sor G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
sor - successive over-relaxation method
.SH CALLING SEQUENCE
.nf
[x,err,iter,flag,res]  = sor(A,b,x0,w,maxi,tol)
.fi
.SH PARAMETERS
.TP
A
: matrix of size n-by-n or function returning \fBA*x\fP
.TP
b
: right hand side vector
.TP
x0
: initial guess vector (default: zeros(n,1))
.TP
w
: relaxation scalar (default: 0.4)
.TP
maxi
: maximum number of iterations (default: n)
.TP
tol
: error tolerance (default: 1000*%eps)
.TP
x
: solution vector
.TP
err
: final residual norm
.TP
iter
: number of iterations performed
.TP
flag
: 0 = \fBsor\fP converged to the desired tolerance within \fBmaxi\fP iterations
.LP
  1 = no convergence given \fBmaxi\fP
.TP
res
: residual vector
.SH DESCRIPTION
Solves the linear system \fBAx=b\fP using the Successive Over-Relaxation Method (Gauss-Seidel method when w = 1 ).
.SH EXAMPLE
.nf
A=makefish(4); b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = sor(A,b,x0)
w=0.8; max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = sor(A,b,x0,w,max_it,tol)

deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = sor(matvec,b,x0,w,max_it,tol)
[x,err,iter,flag,res] = sor(matvec,b,x0)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
jacobi