function A = lehmer(n)
//LEHMER  A = LEHMER(N) is the symmetric positive definite N-by-N matrix with
//                         A(i,j) = i/j for j>=i.
//        A is totally nonnegative.  INV(A) is tridiagonal, and explicit
//        formulas are known for its entries.
//        N <= COND(A) <= 4*N*N.

//        References:
//        M. Newman and J. Todd, The evaluation of matrix inversion
//        programs, J. Soc. Indust. Appl. Math., 6 (1958), pp. 466-476.
//        Solutions to problem E710 (proposed by D.H. Lehmer): The inverse
//        of a matrix, Amer. Math. Monthly, 53 (1946), pp. 534-535.

[lhs,rhs]=argn(0);

if( rhs== 0 ),
   error("Scalar is expected");
end

if ( type(n) ~= 1 ),
   error("Scalar is expected");
end

if ( size(n,1) ~= 1 | size(n,2) ~= 1 ),
   error("n must be a scalar");
end

if ( rhs > 1 ),
   error("Too input arguments");
end

// begin of computations

A = ones(n,1)*(1:n);
A = A./A';
A = tril(A) + tril(A,-1)';

//END lehmer.sci