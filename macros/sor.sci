function [x, err, iter, flag, res]  = sor(Mat, varargin)

//  -- Iterative template routine --
//     Univ. of Tennessee and Oak Ridge National Laboratory
//     October 1, 1993
//     Details of this algorithm are described in "Templates for the
//     Solution of Linear Systems: Building Blocks for Iterative
//     Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra,
//     Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications,
//     1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
//
// [x, err, iter, flag, res]  = sor(A, b, x, w, max_it, tol)
//
// sor.m solves the linear system Ax=b using the 
// Successive Over-Relaxation Method (Gauss-Seidel method when omega = 1 ).
//
// input   Mat      REAL matrix
//         x        REAL initial guess vector
//         b        REAL right hand side vector
//         w        REAL relaxation scalar
//         max_it   INTEGER maximum number of iterations
//         tol      REAL error tolerance
//
// output  x        REAL solution vector
//         err      REAL final residual norm
//         iter     INTEGER number of iterations performed
//         flag     INTEGER: 0 = solution found to tolerance
//                           1 = no convergence given max_it
//         res      REAL residual vector


//========================================================
//========================================================
//
//               Parsing input arguments.
//
//========================================================
//========================================================

[lhs,rhs]=argn(0);
if ( rhs== 0 ),
  error("sor: matrix is expected",502);
end

if (rhs == 1),
   error("Please enter right hand side vector b");
end 

//--------------------------------------------------------
// Parsing of the matrix A
//--------------------------------------------------------

if ( type(Mat) ~=1 & type(Mat) ~=5 ),
   error("sor: matrix is expected",502);
end 

if (size(Mat,1) ~= size(Mat,2)),
   error("sor: matrix A must be square",502);
end

//--------------------------------------------------------
// Parsing of the right hand side b
//--------------------------------------------------------

b=varargin(1);
if ( size(b,2) ~= 1 ),
   error("sor: right hand side member must be a column vector",502);
end

if ( size(b,1) ~= size(Mat,1) ),
   error("sor: right hand side member must have the size of the matrix A",502);
end 
 
//--------------------------------------------------------
// Parsing of the initial vector x
//--------------------------------------------------------

if (rhs >= 3),
  x=varargin(2);
  if (size(x,2) ~= 1),
    error("sor: initial guess x0 must be a column vector",502);
  end
  if ( size(x,1) ~= size(b,1) ),
    error("sor: initial guess x0 must have the size of b",502);
  end 
else
  x=zeros(size(b,1),1);
end

//--------------------------------------------------------
// Parsing of the relaxation argument w
//--------------------------------------------------------

if (rhs >= 4),
  w=varargin(3);
  if (size(w,1) ~= 1 | size(w,2) ~=1),
    error("sor: w must be a scalar",502);
  end 
else
  w=0.4;
end

//--------------------------------------------------------
// Parsing of the maximum number of iterations max_it
//--------------------------------------------------------

if (rhs >= 5),
  max_it=varargin(4);
  if (size(max_it,1) ~= 1 | size(max_it,2) ~=1),
    error("max_it must be a scalar",502);
  end 
else
   max_it=size(b,1);
end

//--------------------------------------------------------
// Parsing of the error tolerance tol
//--------------------------------------------------------

if (rhs == 6),
   tol=varargin(5);
   if (size(tol,1) ~= 1 | size(tol,2) ~=1),
      error("sor: tol must be a scalar",502);
   end
else
   tol=1000*%eps;
end

//--------------------------------------------------------
// test about input arguments number
//--------------------------------------------------------

if (rhs > 6),
   error("sor: too many input arguments",502);
end
   
//========================================================
//========================================================
//
//                Begin of computations
//
//========================================================
//======================================================== 

  flag = 0;                                   // initialization
  i = 0;

  bnrm2 = norm( b );
  if  ( bnrm2 == 0.0 ), bnrm2 = 1.0; end

  r = b - A*x;
  err = norm( r ) / bnrm2;
  res = err;
  if ( err < tol ), return; end

  [ M, N, b ] = split( A, b, w, 2 );          // matrix splitting

  for i = 1:max_it                         // begin iteration

     x_1 = x;
     x   = M \ ( N*x + b );                   // update approximation

     err = norm( x - x_1 ) / norm( x );     // compute error
     res = [res;err];
     if ( err <= tol ), iter=i; break; end          // check convergence

     if ( i == max_it ), iter=i; end
  end
  b = b / w;                                  // restore rhs

  if ( err > tol ), flag = 1; end;           // no convergence

// END sor.sci
