function [ M, N, b ] = split( Mat, varargin )
//
// function [ M, N, b ] = split( A, b, w, flag )
//
// split.m sets up the matrix splitting for the stationary
// iterative methods: jacobi and sor (gauss-seidel when w = 1.0 )
//
// input   A        DOUBLE PRECISION matrix
//         b        DOUBLE PRECISION right hand side vector (for SOR)
//         w        DOUBLE PRECISION relaxation scalar
//         flag     INTEGER flag for method: 1 = jacobi
//                                           2 = sor
//
// output  M        DOUBLE PRECISION matrix
//         N        DOUBLE PRECISION matrix such that A = M - N
//         b        DOUBLE PRECISION rhs vector ( altered for SOR )

[lhs,rhs]=argn(0);

if( rhs== 0 ),
   error("Matrix is expected");
end

if ( type(Mat) ~=1 & type(Mat) ~=5 ),
   error("A must be a matrix");
end

if (size(Mat,1) ~= size(Mat,2)),
   error("The matrix A must be square");
end

if (rhs == 1),
   error("Please enter right hand side vector b");
end  

b=varargin(1);

if( size(b,2) ~= 1 ),
   error("Right hand side member must be a column vector");
end

if ( size(b,1) ~= size(Mat,1) ),
   error("Right hand side member must have the size of the matrix A");
end 
 
if(rhs >= 3),

   w=varargin(2);

   if ( size(w,1) ~= 1 | size(w,2) ~=1 ),
         error("w must be a scalar");
   end 

else
    
   w=0.4;

end

if(rhs == 4),

   flag=varargin(3);

   if ( size(flag,1) ~= 1 | size(flag,2) ~=1 ),
      error("flag must be a scalar");
   end 

else
    
   flag=1;

end

if (rhs > 4),
   error("Too many input arguments");
end

// begin of computations

  [m,n] = size( Mat );
       
  if ( flag == 1 ),                   // jacobi splitting

     M = diag(diag(Mat));
     N = diag(diag(Mat)) - Mat;

  elseif ( flag == 2 ),               // sor/gauss-seidel splitting

     b = w * b;
     M =  w * tril( Mat, -1 ) + diag(diag( Mat ));
     N = -w * triu( Mat,  1 ) + ( 1.0 - w ) * diag(diag( Mat ));

  end;

// END split.sci
