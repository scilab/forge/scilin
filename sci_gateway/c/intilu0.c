#include "stack-c.h"
#include "conv.h"
#include "ilut.h"

extern int intilu0(char *fname)
{ 
  int mA,nA,*ia,iwk,*iw,*jlu,*ju,pierr,mierr=1,nierr=1;
  double *alu;
  SciSparse A,*L,*U;

  CheckRhs(1,1);
  CheckLhs(2,3);
  if (VarType(1)==5){
    GetRhsVar(1,"s",&mA,&nA,&A);    
    if (mA!=nA){
      Scierror(501,"%s: input matrix must be square \r\n",fname);
    }
  } else {
    Scierror(501,"%s: input matrix must be sparse \r\n",fname);
  }
  CreateVar(2,"i",&mierr,&nierr,&pierr);

  iwk=A.nel+A.m+1;
  alu= (double *) malloc(iwk*sizeof(double));
  jlu= (int *) malloc(iwk*sizeof(int));
  ju= (int *) malloc((A.m)*sizeof(int));
  ia= Sci2spk(&A);
  iw= (int *) malloc((A.m)*sizeof(int));

  ilu0_(&A.m,A.R,A.icol,ia,alu,jlu,ju,iw,istk(pierr));
  
  free(iw);
  free(ia);
  
  if (*istk(pierr)!=0){
    free(ju);
    free(jlu);
    free(alu);    
    Scierror(501,"%s: zero pivot encountered at step number %d \r\n",fname,*istk(pierr));
  }
  else {
    spluget(A.m,ju,jlu,alu,&L,&U);
    free(ju);
    free(jlu);
    free(alu);
    CreateVarFromPtr(3,"s",&L->m,&L->n,L);
    CreateVarFromPtr(4,"s",&U->m,&U->n,U);
    LhsVar(1) =3;
    LhsVar(2) =4;
    LhsVar(3) =2;
  }
  return(0);
}
