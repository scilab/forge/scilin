#include "stack-c.h"
#include "conv.h"
#include "ilut.h"

extern int intilud(char *fname)
{ 
  int mA=0,nA=0,*ia,malp=1,nalp=1,mdt=1,ndt=1,mierr=1,nierr=1;  
  int palp,pdt,pierr;
  int iwk, *jw, *jlu, *ju;
  double *w,*alu;
  SciSparse A,*L,*U;

  CheckRhs(1,3);
  CheckLhs(2,3);
  if (VarType(1)==5){
    GetRhsVar(1,"s",&mA,&nA,&A);
    if (mA!=nA){
      Scierror(501,"%s: input matrix must be square \r\n",fname);
    }
  } else {
    Scierror(501,"%s: input matrix must be sparse \r\n",fname);
    return 0;
  }
  if (Rhs>=2) {
    if (VarType(2)==1){
      GetRhsVar(2,"d",&malp,&nalp,&palp);
      if ((malp!=1)||(nalp!=1)){
	Scierror(501,"%s: alpha must be a double \r\n",fname);
      }
    } else {
      Scierror(501,"%s: alpha must be a double \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(2,"d",&malp,&nalp,&palp);
    *stk(palp)=(double) 0.5;
  }
  if (Rhs>=3) {
    if (VarType(3)==1){
      GetRhsVar(3,"d",&mdt,&ndt,&pdt);
      if ((mdt!=1)||(ndt!=1)){
	Scierror(501,"%s: tol must be a double \r\n",fname);
      }
    } else {
      Scierror(501,"%s: tol must be a double \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(3,"d",&mdt,&ndt,&pdt);
    *stk(pdt)=(double) 0.001*eltm(A);
  }
  CreateVar(4,"i",&mierr,&nierr,&pierr);

  iwk=lband(A)+A.m+1;
  jw=(int *) malloc(2*A.m*sizeof(int));
  w= (double *) malloc(A.m*sizeof(double));
  
  alu= (double *) malloc(iwk*sizeof(double));
  jlu= (int *) malloc(iwk*sizeof(int));
  ju= (int *) malloc((A.m)*sizeof(int));
  ia=Sci2spk(&A);
  
  ilud_(&A.m,A.R,A.icol,ia,stk(palp),stk(pdt),alu,jlu,ju,&iwk,w,jw,istk(pierr));
  free(w);
  free(jw);
  free(ia);

  if (*istk(pierr)!=0){
    free(ju);
    free(jlu);
    free(alu);
    if (*istk(pierr)==-1) {
      Scierror(501,"%s: input matrix may be wrong \r\n",fname);
    } else if (*istk(pierr)==-2) {
      Scierror(501,"%s: not enough memory for matrix L or U \r\n",fname);
    } else if (*istk(pierr)==-3) {
      Scierror(501,"%s: zero row encountered \r\n",fname);
    } else {
      Scierror(501,"%s: zero pivot encountered at step number %d \r\n",fname,*istk(pierr));
    }
  }
  else {
    spluget(A.m,ju,jlu,alu,&L,&U);
    free(ju);
    free(jlu);
    free(alu);
    CreateVarFromPtr(5,"s",&L->m,&L->n,L);
    CreateVarFromPtr(6,"s",&U->m,&U->n,U);
    LhsVar(1) =5;
    LhsVar(2) =6;
    LhsVar(3) =4;
  }
  return(0);
}
