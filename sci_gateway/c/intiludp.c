#include "stack-c.h"
#include "conv.h"
#include "ilut.h"

extern int intiludp(char *fname)
{ 
  int mA,nA,*ia,malph=1,nalph=1,mdt=1,ndt=1,i=0,mierr=1,nierr=1;  
  int mptol=1,nptol=1,mbloc=1,nbloc=1,mperm=1,nperm;
  int palph,pdt,pptol,pbloc,pierr,pperm;
  int iwk, *jw, *jlu, *ju, *vperm;
  double *w,*alu;
  SciSparse A,*L,*U;

  CheckRhs(1,5);
  CheckLhs(3,4);
  if (VarType(1)==5){
    GetRhsVar(1,"s",&mA,&nA,&A);
    if (mA!=nA){
      Scierror(501,"%s: input matrix must be square \r\n",fname);
    }
  } else {
    Scierror(501,"%s: input matrix must be sparse \r\n",fname);
    return 0;
  }
  if (Rhs>=2) {
    if (VarType(2)==1){
      GetRhsVar(2,"d",&malph,&nalph,&palph);
      if ((malph!=1)||(nalph!=1)){
	Scierror(501,"%s: alpha must be a double \r\n",fname);
      }
    } else {
      Scierror(501,"%s: alpha must be a double \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(2,"d",&malph,&nalph,&palph);
    *stk(palph)=(double) 0.5;
  }
  if (Rhs>=3) {
    if (VarType(3)==1){
      GetRhsVar(3,"d",&mdt,&ndt,&pdt);
      if ((mdt!=1)||(ndt!=1)){
	Scierror(501,"%s: drop must be a double \r\n",fname);
      }
    } else {
      Scierror(501,"%s: drop must be a double \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(3,"d",&mdt,&ndt,&pdt);
    *stk(pdt)=(double) (0.001*eltm(A));
  }
  if (Rhs>=4) {
    if (VarType(4)==1){
      GetRhsVar(4,"d",&mptol,&nptol,&pptol);
      if ((mptol!=1)||(nptol!=1)){
	Scierror(501,"%s: ptol must be a double \r\n",fname);
      }
    } else {
      Scierror(501,"%s: ptol must be a double \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(4,"d",&mptol,&nptol,&pptol);
    *stk(pptol)=(double) 0.5 ;
  }
  if (Rhs>=5) {
    if (VarType(5)==1){
      GetRhsVar(5,"i",&mbloc,&nbloc,&pbloc);
      if ((mbloc!=1)||(nbloc!=1)){
	Scierror(501,"%s: bloc must be an integer \r\n",fname);
      }
    } else {
      Scierror(501,"%s: bloc must be an integer \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(5,"i",&mbloc,&nbloc,&pbloc);
    *istk(pbloc)=(int) A.m;
  }
  CreateVar(6,"i",&mierr,&nierr,&pierr);
  nperm=A.m;
  CreateVar(7,"i",&mperm,&nperm,&pperm);

  iwk=lband(A)+1;
  jw=(int *) malloc(2*A.m*sizeof(int));
  w= (double *) malloc(2*A.m*sizeof(double));
  
  alu= (double *) malloc(iwk*sizeof(double));
  jlu= (int *) malloc(iwk*sizeof(int));
  ju= (int *) malloc((A.m)*sizeof(int));
  
  ia=Sci2spk(&A);
  vperm= (int *) malloc(2*A.m*sizeof(int));
  
  iludp_(&A.m,A.R,A.icol,ia,stk(palph),stk(pdt),stk(pptol),istk(pbloc),alu,jlu,ju,&iwk,w,jw,vperm,istk(pierr));

  free(w);
  free(jw);
  free(ia);
  
  if (*istk(pierr)!=0){
    free(ju);
    free(jlu);
    free(alu);
    free(vperm);
    if (*istk(pierr)==-1) {
      Scierror(501,"%s: input matrix may be wrong \r\n",fname);
    } else if (*istk(pierr)==-2) {
      Scierror(501,"%s: not enough memory for matrix L or U \r\n",fname);
    } else if (*istk(pierr)==-3) {
      Scierror(501,"%s: zero row encountered \r\n",fname);
    } else {
      Scierror(501,"%s: zero pivot encountered at step number %d \r\n",fname,*istk(pierr));
    }
  }
  else {
    for (i=0;i<A.m;i++)
      istk(pperm)[i]=vperm[i];
    free(vperm);

    spluget(A.m,ju,jlu,alu,&L,&U);
    free(ju);
    free(jlu);
    free(alu);
    CreateVarFromPtr(8,"s",&L->m,&L->n,L);
    CreateVarFromPtr(9,"s",&U->m,&U->n,U);

    LhsVar(1) =8;
    LhsVar(2) =9;
    LhsVar(3) =7;
    LhsVar(4) =6;
  }
  return(0);
}
