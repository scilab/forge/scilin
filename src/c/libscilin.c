#include <mex.h> 
extern Gatefunc intilu0;
extern Gatefunc intilut;
extern Gatefunc intilutp;
extern Gatefunc intiluk;
extern Gatefunc intilud;
extern Gatefunc intiludp;
extern Gatefunc intmilu0;
extern Gatefunc intsplsolve;
extern Gatefunc intspusolve;
extern Gatefunc inttriangular;
extern Gatefunc intmminfo;
extern Gatefunc intmmread;
extern Gatefunc intmmwrite;
extern Gatefunc intgen57pt;

static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,intilu0,"ilu0"},
  {(Myinterfun)sci_gateway,intilut,"ilut"},
  {(Myinterfun)sci_gateway,intilutp,"ilutp"},
  {(Myinterfun)sci_gateway,intiluk,"iluk"},
  {(Myinterfun)sci_gateway,intilud,"ilud"},
  {(Myinterfun)sci_gateway,intiludp,"iludp"},
  {(Myinterfun)sci_gateway,intmilu0,"milu0"},
  {(Myinterfun)sci_gateway,intsplsolve,"splsolve"},
  {(Myinterfun)sci_gateway,intspusolve,"spusolve"},
  {(Myinterfun)sci_gateway,inttriangular,"triangular"},
  {(Myinterfun)sci_gateway,intmminfo,"mminfo"},
  {(Myinterfun)sci_gateway,intmmread,"mmread"},
  {(Myinterfun)sci_gateway,intmmwrite,"mmwrite"},
  {(Myinterfun)sci_gateway,intgen57pt,"gen57pt"},
};

int C2F(libscilin)()
{
  Rhs = Max(0, Rhs);
  (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  return 0;
}
